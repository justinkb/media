Title: media-libs/lv2core obsoleted by media-libs/lv2
Author: Ali Polatel <alip@exherbo.org>
Content-Type: text/plain
Posted: 2013-01-27
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: media-libs/lv2core

The package media-libs/lv2core is obsoleted by media-libs/lv2. Many, if not
all, packages depending on media-libs/lv2core have been updated and revision
bumped. Please uninstall media-libs/lv2core and install media-libs/lv2.
