# Copyright 2013-2018 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require openrc-service [ openrc_confd_files=[ "${FILES}"/openrc/confd ] ]

export_exlib_phases src_unpack src_prepare src_install

MY_PN=${PN/-bin}
MY_PN_C=${MY_PN//-}
MY_V=$(ever range 1-5)
SUMMARY="Media server and player"
BASE_URI="plex.tv"
HOMEPAGE="http://${BASE_URI}"

DOWNLOADS="
    platform:amd64? ( http://downloads.${BASE_URI}/${MY_PN}/${MY_V}-${HEX_CRAP}/${MY_PN_C}-${MY_V}-${HEX_CRAP}.x86_64.rpm )
    platform:x86? ( http://downloads.${BASE_URI}/${MY_PN}/${MY_V}-${HEX_CRAP}/${MY_PN_C}-${MY_V}-${HEX_CRAP}.i386.rpm )
"

UPSTREAM_DOCUMENTATION="
    https://support.${BASE_URI}/hc/en-us [[
        lang = en description = [ Documenation central ]
    ]]
    https://support.${BASE_URI}/hc/en-us/sections/200056078-Quick-Start-Guide [[
        lang = en description = [ Quick Start Guide ]
    ]]
"

LICENCES="PMS"
SLOT="0"
MYOPTIONS="platform: amd64 x86"

RESTRICT="mirror"

DEPENDENCIES="
    build:
        app-arch/rpm2targz
    build+run:
        group/plex
        user/plex
"

plex-media-server-bin_src_unpack() {
    # Unpack the rpm
    edo /usr/$(exhost --target)/bin/rpmunpack "${FETCHEDDIR}/${ARCHIVES}"

    # Simplify WORK
    edo pushd "${WORKBASE}"
    edo mv ${MY_PN_C}-${MY_V}-${HEX_CRAP}.* ${PNV}
    edo popd
}

plex-media-server-bin_src_prepare() {
    # Clean out all Fedora stuff
    if ! ever at_least 1.3.4.3285 ; then
        edo rm -r ./usr/local
    fi
    edo rm -r ./usr/share
    edo rm -r ./etc/init.d
    edo rm -r ./etc/yum.repos.d
    edo rm -r ./etc/zypp
    edo rm -r ./etc/sysconfig

    # Fix service file
    edo sed -i -e "s:\(/usr\)\(/lib\):\1/$(exhost --target)\2:g" lib/systemd/system/plexmediaserver.service

    # Move some things around
    edo mkdir -p "${WORK}"/usr/$(exhost --target)/lib
    edo mv ./lib/* ./usr/$(exhost --target)/lib
    edo cp -a ./usr/lib ./usr/$(exhost --target)
    edo rm -rf ./usr/lib

    # Remove empty dirs
    edo find . -type d -empty -delete
}

plex-media-server-bin_src_install() {
    # Move WORK's contents to IMAGE
    edo mv "${WORK}"/* "${IMAGE}"

    keepdir /var/lib/plexmediaserver
    edo chmod g+w "${IMAGE}"/var/lib/plexmediaserver
    edo chown plex:plex "${IMAGE}"/var/lib/plexmediaserver

    install_openrc_files
}

