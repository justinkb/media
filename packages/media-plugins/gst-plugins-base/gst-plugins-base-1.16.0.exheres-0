# Copyright 2008-2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Copyright 2019 Danilo Spinella <danyspin97@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require gst-plugins-base test-dbus-daemon meson

PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS+="
    debug
    doc
    gobject-introspection
    X
    wayland
    gstreamer_plugins:opengl? (
        ( X wayland ) [[ number-selected = at-least-one ]]
        ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    )
    gstreamer_plugins:
        alsa      [[ description = [ Audio input, output and mixing with ALSA ] ]]
        cdda      [[ description = [ Robust CD audio extraction using cdparanoia ] ]]
        graphene  [[ description = [ OpenGL based video transformation plugins using Graphene ]
                     requires    = [ gstreamer_plugins: opengl ] ]]
        libvisual [[ description = [ Audio visualization using libvisual ] ]]
        ogg       [[ description = [ Ogg multimedia container format support ] ]]
        opengl    [[ description = [ OpenGL video sink ] ]]
        opus      [[ description = [ Opus audio decoding and encoding ] ]]
        pango     [[ description = [ Pango-based text rendering and video overlaying (needed to display subtitles) ] ]]
        theora    [[ description = [ Theora video encoding and decoding using libtheora ] ]]
        vorbis    [[ description = [ Vorbis audio support using libvorbis ] ]]
        xv        [[ description = [ Video output using the XVideo extension ] ]]
"

# Tests require access to Network and X-Server
RESTRICT="test"

DEPENDENCIES+="
    build:
        virtual/pkg-config[>=0.20]
        doc? ( dev-doc/gtk-doc[>=1.12] )
    build+run:
        app-text/iso-codes
        dev-libs/glib:2[>=2.40.0]
        dev-libs/orc:0.4[>=0.4.24]
        media-libs/gstreamer:1.0[>=${PV}][gobject-introspection?]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.31.1] )
        gstreamer_plugins:alsa? ( sys-sound/alsa-lib[>=0.9.1] )
        gstreamer_plugins:cdda? ( media/cdparanoia[>=0.10.2] )
        gstreamer_plugins:graphene? ( x11-libs/graphene:1.0[>=1.4.0]  )
        gstreamer_plugins:libvisual? ( media-libs/libvisual:0.4 )
        gstreamer_plugins:ogg? ( media-libs/libogg[>=1.0] )
        gstreamer_plugins:opengl? (
            media-libs/libpng:=[>=1.0]
            x11-dri/glu
            x11-dri/libdrm
            x11-dri/mesa[X?][wayland?]
        )
        gstreamer_plugins:opus? ( media-libs/opus[>=0.9.4] )
        gstreamer_plugins:pango? ( x11-libs/pango[>=1.22.0] )
        gstreamer_plugins:theora? ( media-libs/libtheora[>=1.1] )
        gstreamer_plugins:vorbis? ( media-libs/libvorbis[>=1.0] )
        gstreamer_plugins:xv? ( x11-libs/libXv )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        X? (
            x11-libs/libX11
            x11-libs/libXext
            x11-libs/libxcb
        )
        wayland? (
            sys-libs/wayland[>=1.0]
            sys-libs/wayland-protocols[>=1.15]
        )
        !media-libs/gst-plugins-bad:1.0[<=1.14.0] [[
            description = [ gstcompositor is now provided by gst-plugins-base ]
            resolution = upgrade-blocked-before
        ]]
"

MESON_SRC_CONFIGURE_PARAMS=(
    # core plugins
    -Dadder=enabled
    -Dapp=enabled
    -Daudioconvert=enabled
    -Daudiomixer=enabled
    -Daudiorate=enabled
    -Daudioresample=enabled
    -Daudiotestsrc=enabled
    -Dcompositor=enabled
    -Dencoding=enabled
    -Dgio=enabled
    -Doverlaycomposition=enabled
    -Dpbtypes=enabled
    -Dplayback=enabled
    -Drawparse=enabled
    -Dsubparse=enabled
    -Dtcp=enabled
    -Dtypefind=enabled
    -Dvideoconvert=enabled
    -Dvideorate=enabled
    -Dvideoscale=enabled
    -Dvideotestsrc=enabled
    -Dvolume=enabled

    # don't compile not installed examples
    -Dexamples=disabled

    -Diso-codes=enabled
    -Dnls=enabled
    -Dorc=enabled
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'debug glib-asserts'
    'debug glib-checks'
    'debug gobject-cast-checks'
    'doc gtk_doc'
    'gobject-introspection introspection'
    'X x11'
    'X xshm'

    # optional plugins
    'gstreamer_plugins:alsa'
    'gstreamer_plugins:cdda cdparanoia'
    'gstreamer_plugins:graphene gl-graphene'
    'gstreamer_plugins:libvisual'
    'gstreamer_plugins:ogg'
    'gstreamer_plugins:opengl gl'
    'gstreamer_plugins:opengl gl-jpeg'
    'gstreamer_plugins:opengl gl-png'
    'gstreamer_plugins:opus'
    'gstreamer_plugins:pango'
    'gstreamer_plugins:theora'
    'gstreamer_plugins:vorbis vorbis'
    'gstreamer_plugins:xv xshm'
    'gstreamer_plugins:xv xvideo'
)

src_configure() {
    local gl_api=gles2
    local gl_platform=egl
    local gl_winsys=
    if option X; then
        gl_api+=,opengl
        gl_platform+=,glx
        gl_winsys+=x11
    fi
    if option wayland; then
        if option X; then
            gl_winsys+=,
        fi
        gl_winsys+=wayland
    fi

    MESON_SRC_CONFIGURE_PARAMS+=(
        -Dgl_api=${gl_api}
        -Dgl_platform=${gl_platform}
        -Dgl_winsys=${gl_winsys}
    )

    meson_src_configure
}

