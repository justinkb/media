# Copyright 2010-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

# Pretty much dead but the official repository of the current maintainer
#require github [ tag=RELEASE_${PV//./_} ] cmake [ api=2 ]

# The original author came back and picked up the pieces
require github [ user=Tulon tag=EXPERIMENTAL_2016_02_22 ] cmake [ api=2 ]

SUMMARY="Interactive post-processing tool for scanned pages (of text)"
DESCRIPTION="
Scan Tailor is an interactive post-processing tool for scanned pages. It performs
operations such as page splitting, deskewing, adding/removing borders, and others.
You give it raw scans, and you get pages ready to be printed or assembled into a
PDF or DJVU file. Scanning, optical character recognition (use tesseract), and
assembling multi-page documents are out of scope of this project.
"
HOMEPAGE="http://${PN}.org https://github.com/Tulon/scantailor"
BUGS_TO="philantrop@exherbo.org"

UPSTREAM_DOCUMENTATION="http://${PN}.wiki.sourceforge.net/"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    opencl
    opengl
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        x11-proto/xorgproto
    build+run:
        dev-libs/boost[>=1.35]
        media-libs/libpng:=
        media-libs/tiff
        sci-libs/eigen:3[>=2.91.0]
        x11-libs/libXrender
        x11-libs/qtbase:5[>=5.3][gui]
        opencl? ( dev-libs/ocl-icd )
        opengl? ( x11-dri/mesa )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
"

# The tests need a running X server.
RESTRICT="test"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_INSTALL_PREFIX=/usr
    -DCMAKE_SYSTEM_PREFIX_PATH:PATH="${PREFIX:=/usr/$(exhost --target)}"
#    -DENABLE_DEWARPING=ON
)

CMAKE_SRC_CONFIGURE_OPTION_ENABLES=(
    "opencl OPENCL"
    "opengl OPENGL"
)

src_prepare() {
    cmake_src_prepare

    edo sed -i -e "s:\(DESTINATION\) bin:\1 /usr/$(exhost --target)/bin:" CMakeLists.txt
    edo sed -i -e "s:/lib/\(scantailor\):/$(exhost --target)/lib/\1:" CMakeLists.txt
    edo sed -i -e "s:\(DESTINATION\) lib:\1 /usr/$(exhost --target)/lib:" {math,imageproc,foundation,dewarping,acceleration}/CMakeLists.txt
    edo sed -i -e "s:\(DESTINATION\) lib:\1 /usr/$(exhost --target)/lib:" acceleration/opencl/CMakeLists.txt
}

