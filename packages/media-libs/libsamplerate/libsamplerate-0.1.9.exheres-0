# Copyright 2009 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

SUMMARY="Secret Rabbit Code (aka libsamplerate) is a Sample Rate Converter for audio"
DESCRIPTION="
Secret Rabbit Code (aka libsamplerate) is a Sample Rate Converter for audio.
One example of where such a thing would be useful is converting audio from the
CD sample rate of 44.1kHz to the 48kHz sample rate used by DAT players.

SRC is capable of arbitrary and time varying conversions ; from downsampling by
a factor of 256 to upsampling by the same factor. Arbitrary in this case means
that the ratio of input and output sample rates can be an irrational number.
The conversion ratio can also vary with time for speeding up and slowing down effects.

SRC provides a small set of converters to allow quality to be traded off
against computation cost. The current best converter provides a signal-to-noise
ratio of 145dB with -3dB passband extending from DC to 96% of the theoretical
best bandwidth for a given pair of input and output sample rates.
"
HOMEPAGE="http://www.mega-nerd.com/SRC"
DOWNLOADS="${HOMEPAGE}/${PNV}.tar.gz"

REMOTE_IDS="freecode:${PN}"

UPSTREAM_CHANGELOG="${HOMEPAGE}/ChangeLog [[ lang = en ]]"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/api.html [[ lang = en ]]"
UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/history.html [[ lang = en ]]"

LICENCES="BSD-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="examples"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
        examples? ( sci-libs/fftw[>=0.15.0] )
    build+run:
        media-libs/libsndfile[>=1.0.6]
        sys-sound/alsa-lib
    test:
        sci-libs/fftw[>=0.15.0]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-sndfile
    --disable-static
)

src_install() {
    default

    emagicdocs

    insinto /usr/share/doc/${PNVR}/html
    doins doc/*.{css,html,png}

    if option examples; then
        insinto /usr/share/doc/${PNVR}/examples
        doins examples/*.c
    fi

    edo rm -r "${IMAGE}"/usr/$(exhost --target)/share
}

